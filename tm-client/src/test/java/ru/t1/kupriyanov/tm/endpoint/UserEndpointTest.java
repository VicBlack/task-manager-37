package ru.t1.kupriyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kupriyanov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kupriyanov.tm.api.endpoint.IUserEndpoint;
import ru.t1.kupriyanov.tm.dto.request.*;
import ru.t1.kupriyanov.tm.dto.response.UserLoginResponse;
import ru.t1.kupriyanov.tm.dto.response.UserRegistryResponse;
import ru.t1.kupriyanov.tm.dto.response.UserRemoveResponse;
import ru.t1.kupriyanov.tm.dto.response.UserUpdateProfileResponse;
import ru.t1.kupriyanov.tm.marker.ISoapCategory;
import ru.t1.kupriyanov.tm.model.User;

import java.util.UUID;

@Category(ISoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void getTokens() {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        @NotNull final UserLoginResponse userResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        adminToken = adminResponse.getToken();
        userToken = userResponse.getToken();
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(null, "testLogin", "testPassword", "testEmail")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest("", "testLogin", "testPassword", "testEmail")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, null, "testPassword", "testEmail")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, "testLogin", null, "testEmail")
        ));

        @Nullable final UserRegistryResponse response = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "adminUserToDelete", "passwordToDelete", "emailToDelete")
        );

        Assert.assertNotNull(response.getUser());
        @Nullable final User user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(response.getUser().getLogin(), user.getLogin());

        @Nullable final UserLoginResponse login = authEndpoint.login(
                new UserLoginRequest(response.getUser().getLogin(), "passwordToDelete")
        );
        Assert.assertTrue(login.getSuccess());
        @Nullable final UserRemoveResponse removeResponse = userEndpoint.removeUser(
                new UserRemoveRequest(adminToken, "adminUserToDelete")
        );
        Assert.assertNotNull(removeResponse.getUser());
    }

    @Test
    public void updateUserProfile() {
        @NotNull final String firstName = "changedFirstName";
        @NotNull final String lastName = "changedLastName";
        @NotNull final String middleName = "changedMiddleName";
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(null, firstName, lastName, middleName)
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest("", firstName, lastName, middleName)
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest("token", firstName, lastName, middleName)
        ));
        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(userToken, firstName, lastName, middleName)
        );
        Assert.assertNotNull(response.getUser());
        @Nullable final User user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(lastName, user.getLastName());
    }

    @Test
    public void removeUser() {
        UserRegistryResponse userRegistryResponse = userEndpoint.registryUser(new UserRegistryRequest(
                adminToken,
                "userToDelete" + UUID.randomUUID().toString(),
                "passwordToDelete",
                "emailToDelete")
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(null, "testUserToDelete")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest("", "testUserToDelete")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(userToken, null)
        ));
        @Nullable final UserRemoveResponse response = userEndpoint.removeUser(
                new UserRemoveRequest(adminToken, userRegistryResponse.getUser().getLogin())
        );
        Assert.assertNotNull(response.getUser());
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(adminToken, userRegistryResponse.getUser().getLogin())
        ));
    }

}
