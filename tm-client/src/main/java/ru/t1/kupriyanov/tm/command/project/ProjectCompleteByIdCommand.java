package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

}
