package ru.t1.kupriyanov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IProjectRepository;
import ru.t1.kupriyanov.tm.api.service.IConnectionService;
import ru.t1.kupriyanov.tm.api.service.IProjectTaskService;
import ru.t1.kupriyanov.tm.api.repository.ITaskRepository;
import ru.t1.kupriyanov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kupriyanov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kupriyanov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.TaskIdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.UserIdEmptyException;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.model.Task;
import ru.t1.kupriyanov.tm.repository.ProjectRepository;
import ru.t1.kupriyanov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

   @NotNull
   private final IProjectRepository getProjectRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
   }

    @NotNull
    private final ITaskRepository getTaskRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(userId, task);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final Project project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.removeOneById(userId, projectId);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(userId, task);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

}
