package ru.t1.kupriyanov.tm.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IRepository;
import ru.t1.kupriyanov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    protected abstract String getTableName();

    @NotNull
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        for (final M model: models) add(model);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format(
                "DELETE FROM %s",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rows = statement.executeQuery(sql);
            while (rows.next()) result.add(fetch(rows));
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE ID = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rows = statement.executeQuery();
            if (!rows.next()) return null;
            return fetch(rows);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ?;",
                getTableName(),
                "ROW_NUMBER"
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet rows = statement.executeQuery();
            if (!rows.next()) return null;
            return fetch(rows);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        @NotNull final String sql = String.format(
                "DELETE FROM %s where ID = ?,",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll() {
        clear();
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String query = String.format(
                "SELECT COUNT(1) FROM %s;",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet rows = statement.executeQuery(query)) {
            rows.next();
            return rows.getInt("COUNT");
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
