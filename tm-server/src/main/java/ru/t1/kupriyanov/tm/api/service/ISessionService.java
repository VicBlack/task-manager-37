package ru.t1.kupriyanov.tm.api.service;

import ru.t1.kupriyanov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
