package ru.t1.kupriyanov.tm.api.repository;

import ru.t1.kupriyanov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
