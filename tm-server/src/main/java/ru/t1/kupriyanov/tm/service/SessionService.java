package ru.t1.kupriyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.api.repository.ISessionRepository;
import ru.t1.kupriyanov.tm.api.service.IConnectionService;
import ru.t1.kupriyanov.tm.api.service.ISessionService;
import ru.t1.kupriyanov.tm.model.Session;
import ru.t1.kupriyanov.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
