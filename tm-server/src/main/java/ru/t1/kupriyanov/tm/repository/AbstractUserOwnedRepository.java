package ru.t1.kupriyanov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kupriyanov.tm.model.AbstractUserOwnedModel;

import javax.xml.ws.BindingType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final String userId, @NotNull final M model);

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> list = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s where USER_ID = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rows = statement.executeQuery();
            while(rows.next()) list.add(fetch(rows));
        }
        return list;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null) return null;
        @Nullable final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE ID = ? AND USER_ID = ? LIMIT 1;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rows = statement.executeQuery();
            if (!rows.next()) return null;
            return fetch(rows);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE ID = ? AND ROW_NUMBER = ? LIMIT 1;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet rows = statement.executeQuery();
            if (!rows.next()) return null;
            return fetch(rows);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final String userId, @NotNull final M model) {
        if (userId == null) return null;
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE ID = ? AND USER_ID = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId) {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE USER_ID = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final String sql = String.format(
                "SELECT COUNT(1) AS COUNT FROM %s WHERE USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            rowSet.next();
            return rowSet.getInt("COUNT");
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

}
