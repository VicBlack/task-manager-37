package ru.t1.kupriyanov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IProjectRepository;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.model.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Getter
    @NotNull
    private final String tableName = "T1_PROJECTS";

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    @NotNull
    @SneakyThrows
    public Project fetch(@NotNull final ResultSet rows) {
        @NotNull final Project project = new Project();
        project.setUserId(rows.getString("USER_ID"));
        project.setId(rows.getString("ID"));
        project.setName(rows.getString("NAME"));
        project.setDescription(rows.getString("DESCRIPTION"));
        project.setStatus(Status.toStatus(rows.getString("STATUS")));
        project.setCreated(rows.getTimestamp("CREATED"));
        return project;
    }

    @NotNull
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (USER_ID, ID, NAME, DESCRIPTION, STATUS, CREATED) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getUserId());
            statement.setString(2, project.getId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().name());
            statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @SneakyThrows
    public Project add(@Nullable final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @SneakyThrows
    public Project update(@Nullable final String userId, @NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s " +
                        "SET NAME = ?," +
                        "DESCRIPTION = ?" +
                        "WHERE ID = ? AND USER_ID = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getId());
            statement.setString(4, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE USER_ID = ?;",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rows = statement.executeQuery(sql);
            while (rows.next()) result.add(fetch(rows));
        }
        return result;
    }

}
