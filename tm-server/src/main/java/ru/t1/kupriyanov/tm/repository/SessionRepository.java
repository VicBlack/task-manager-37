package ru.t1.kupriyanov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.ISessionRepository;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Getter
    @NotNull
    private final String tableName = "T1_SESSIONS";

    public SessionRepository(Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("ID"));
        session.setUserId(row.getString("USER_ID"));
        session.setRole(Role.toRole(row.getString("ROLE")));
        session.setDate(row.getTimestamp("LAST_DATE"));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        if (session.getRole() == null) session.setRole(Role.USUAL);
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, USER_ID, ROLE) VALUES (?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().name());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @SneakyThrows
    public Session add(@Nullable final String userId, @NotNull final Session session) {
        session.setUserId(userId);
        return add(session);
    }

}
