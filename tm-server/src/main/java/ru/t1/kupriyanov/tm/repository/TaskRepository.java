package ru.t1.kupriyanov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.ITaskRepository;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.UserIdEmptyException;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.model.Task;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Getter
    @NotNull
    private final String tableName = "T1_TASKS";

    public TaskRepository(Connection connection) {
        super(connection);
    }

    @NotNull
    @SneakyThrows
    public Task fetch(@NotNull final ResultSet rows) {
        @NotNull final Task task = new Task();
        task.setUserId(rows.getString("USER_ID"));
        task.setId(rows.getString("ID"));
        task.setProjectId(rows.getString("PROJECT_ID"));
        task.setName(rows.getString("NAME"));
        task.setDescription(rows.getString("DESCRIPTION"));
        task.setStatus(Status.toStatus(rows.getString("STATUS")));
        task.setCreated(rows.getTimestamp("CREATED"));
        return task;
    }

    @NotNull
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (USER_ID, ID, PROJECT_ID, NAME, DESCRIPTION, STATUS, CREATED) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getUserId());
            statement.setString(2, task.getId());
            statement.setString(3, task.getProjectId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setString(6, task.getStatus().name());
            statement.setTimestamp(7, new Timestamp(task.getCreated().getTime()));
        }
        return task;
    }

    @NotNull
    @SneakyThrows
    public Task add(@Nullable final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @SneakyThrows
    public Task update(@NotNull final String userId, @NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s " +
                        "SET NAME = ?," +
                        "DESCRIPTION = ?," +
                        "PROJECT_ID = ?," +
                        "WHERE ID = ? AND USER_ID = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getProjectId());
            statement.setString(4, task.getId());
            statement.setString(5, task.getUserId());
            statement.executeUpdate();
        }
        return task;
    }

//    @NotNull
//    @Override
//    public Task create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
//        @NotNull final Task task = new Task();
//        task.setUserId(userId);
//        task.setName(name);
//        task.setDescription(description);
//        return add(task);
//    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE PROJECT_ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

}
