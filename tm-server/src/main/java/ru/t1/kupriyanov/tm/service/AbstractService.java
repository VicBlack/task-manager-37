package ru.t1.kupriyanov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IRepository;
import ru.t1.kupriyanov.tm.api.service.IConnectionService;
import ru.t1.kupriyanov.tm.api.service.IService;
import ru.t1.kupriyanov.tm.exception.entity.ModelNotFoundException;
import ru.t1.kupriyanov.tm.exception.field.IdEmptyException;
import ru.t1.kupriyanov.tm.exception.field.IndexIncorrectException;
import ru.t1.kupriyanov.tm.model.AbstractModel;

import javax.xml.ws.ServiceMode;
import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneByIndex(index);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeOne(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M model;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.removeOneById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M model;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.removeOneByIndex(index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        int size;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            size = repository.getSize();
        }
        return size;
    }

    @Override
    @SneakyThrows
    public boolean existsById(final @NotNull String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

}
