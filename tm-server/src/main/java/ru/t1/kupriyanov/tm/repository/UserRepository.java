package ru.t1.kupriyanov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.repository.IUserRepository;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.model.Task;
import ru.t1.kupriyanov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    @NotNull
    private final String tableName = "T1_USERS";

    public UserRepository(Connection connection) {
        super(connection);
    }

    @NotNull
    @SneakyThrows
    public User fetch(@NotNull final ResultSet rows) {
        @NotNull final User user = new User();
        user.setId(rows.getString("ID"));
        user.setLogin(rows.getString("LOGIN"));
        user.setPasswordHash(rows.getString("PASSWORD_HASH"));
        user.setFirstName(rows.getString("FIRST_NAME"));
        user.setMiddleName(rows.getString("MIDDLE_NAME"));
        user.setLastName(rows.getString("LAST_NAME"));
        user.setEmail(rows.getString("EMAIL"));
        user.setRole(Role.toRole(rows.getString("ROLE")));
        user.setLocked(rows.getBoolean("LOCKED"));
        return user;
    }

    @NotNull
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD_HASH, FIRST_NAME, MIDDLE_NAME, " +
                        "LAST_NAME, EMAIL, ROLE, LOCKED) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().name());
            statement.setString(9, user.getLocked().toString());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s " +
                        "SET LOGIN = ?," +
                        "PASSWORD_HASH = ?" +
                        "FIRST_NAME = ?" +
                        "MIDDLE_NAME = ?" +
                        "LAST_NAME = ?" +
                        "EMAIL = ?" +
                        "ROLE = ?" +
                        "LOCKED = ?" +
                        "WHERE ID = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().name());
            statement.setString(9, user.getLocked().toString());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE LOGIN = ? LIMIT 1;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE EMAIL = ? LIMIT 1;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByLogin(email) != null;
    }

}
